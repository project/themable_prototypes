// For production, a build step will inline this React dependency into the
// distribution bundle.
import React from ' https://unpkg.com/react@17/umd/react.production.min.js';
import ReactDOM from 'https://unpkg.com/react-dom@17/umd/react-dom.production.min.js';

(function (Drupal) {
  // Shorter names for passing to theme functions.
  const h = React.createElement;
  const Fragment = React.Fragment;

  // The code in this class can be complex. Themes should be able to override
  // the rendered markup without having to fork or extend this code.
  class ToDoList extends React.Component {
    constructor(props) {
      super(props);
      // This example is from https://lit.dev/tutorial/#05, modified for React.
      this.state = {
        listItems = [
          {text: 'Start Lit tutorial', completed: true},
          {text: 'Make to-do list', completed: false},
        ];
      }
    }

    render() {
      return Drupal.theme.todoList(h, Fragment, this.state.listItems);
    }
  }

  ReactDOM.render(<ToDoList/>, document.getElementById('todoList'));

  // Only this is what themes should override. The override might already be
  // defined before this runs.
  Drupal.theme.todoList = Drupal.theme.todoList || ((h, Fragement, items) => {
    return (
      <>
        <h2>To Do</h2>
        <ul>
          {items.map((item) =>
            <li>{item.text}</li>
          )}
        </ul>
      </>
    );
  });

})(Drupal);
