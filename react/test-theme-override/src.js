(function (Drupal) {

  // Override the UI's unordered list to an ordered list. Notice that nothing
  // in this JS file depends on React: that's entirely internal to test-ui.js.
  // The only requirement is that we're passed the 'h' and 'Fragment' functions
  // with which Babel can transform the JSX. test-ui.js happens to pass the
  // React functions for this, but if it ever wants to switch from React to
  // Preact or a different JSX renderer, then it could pass the h and Fragment
  // functions from that library, without anything here needing to change.
  Drupal.theme.todoList = (h, Fragment, items) => {
    return (
      <>
        <h2>To Do</h2>
        <ol>
          {items.map((item) =>
            <li>{item.text}</li>
          )}
        </ol>
      </>
    );
  }

})(Drupal);
