(function (Drupal) {

  // Override the UI's unordered list to an ordered list. Notice that nothing
  // in this JS file depends on Lit: that's entirely internal to test-ui.js.
  // The only requirement is that we're passed an 'html' function with which to
  // tag template literals. test-ui.js happens to pass Lit's html function for
  // this, but if it ever wants to switch from Lit to a different library, then
  // it could pass a different function, without anything here needing to
  // change.
  Drupal.theme.todoList = (html, items) => {
    return html`
      <h2>To Do</h2>
      <ol>
        ${items.map((item) =>
          html`<li>${item.text}</li>`
        )}
      </ol>
    `;
  }

})(Drupal);
