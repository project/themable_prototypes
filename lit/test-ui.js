// For production, a build step will inline this Lit dependency into the
// distribution bundle.
import {LitElement, html} from 'https://cdn.jsdelivr.net/gh/lit/dist@2/core/lit-core.min.js';

(function (Drupal) {
  // The code in this class can be complex. Themes should be able to override
  // the rendered markup without having to fork or extend this code.
  class ToDoList extends LitElement {
    static properties = {
      listItems: {},
    };

    constructor() {
      super();
      // This example is from https://lit.dev/tutorial/#05.
      this.listItems = [
        {text: 'Start Lit tutorial', completed: true},
        {text: 'Make to-do list', completed: false},
      ];
    }

    render() {
      return Drupal.theme.todoList(html, this.listItems);
    }
  }

  customElements.define('todo-list', ToDoList);

  // Only this is what themes should override. The override might already be
  // defined before this runs.
  Drupal.theme.todoList = Drupal.theme.todoList || ((html, items) => {
    return html`
      <h2>To Do</h2>
      <ul>
        ${items.map((item) =>
          html`<li>${item.text}</li>`
        )}
      </ul>
    `;
  });

})(Drupal);
